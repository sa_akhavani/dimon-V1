import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import Jama.Matrix;

public class Encoder {

	static int amountReduceFactor = 10; // Factor to divide amount by

	static int idNumberOfBits 				= 32; // Number of bits used to show id of users
	static int randomIdNumberOfBits			= 8;  // Number of bits used to show the random id generated for this transaction
	static int reducedAmountNumberOfBits 	= 16; // Number of bits used to show the reduced amount (amount / amountReduceFactor)

	// Name of file containing generator matrix size
	static String systematicGeneratorMatrixSizeFileName = "misc/systematicGeneratorMatrixSize.txt";
	// Name of file containing generator matrix
	static String systematicGeneratorMatrixFileName = "misc/systematicGeneratorMatrix.txt";

	static String transmitFilePath = "transmit/";
	static String transmitFileNamePrefix = "transmit";
	static String transmitFileExtension = "wav";

	public static int getAmountReduceFactor() {
		return amountReduceFactor;
	}

	public static int getIdNumberOfBits() {
		return idNumberOfBits;
	}

	public static int getRandomIdNumberOfBits() {
		return randomIdNumberOfBits;
	}
	
	public static int getReducedAmountNumberOfBits() {
		return reducedAmountNumberOfBits;
	}
	
	public static String getSystematicGeneratorMatrixSizeFileName() {
		return systematicGeneratorMatrixSizeFileName;
	}

	public static String getSystematicGeneratorMatrixFileName() {
		return systematicGeneratorMatrixFileName;
	}

	public static String getTransmitFilePath() {
		return transmitFilePath;
	}
	
	public static String getTransmitFileNamePrefix() {
		return transmitFileNamePrefix;
	}
	
	public static String getTransmitFileExtension() {
		return transmitFileExtension;
	}

	public static String generateTransmitFileName(String mIdString) {
		DateFormat dateFormat = new SimpleDateFormat("ddMMyy_HHmmss");
		Calendar calendarObj = Calendar.getInstance();
		String dateAndTimeString = dateFormat.format(calendarObj.getTime());
		String transmitFileName = getTransmitFilePath() + getTransmitFileNamePrefix() +
				"_" + mIdString + "_" + dateAndTimeString + "." + getTransmitFileExtension();
		return transmitFileName;
	}

	/**
	 * This function generates binary message string from ID and amount.
	 * 
	 * @param mIdString
	 * @param mAmountString
	 * @return binary message string (ID + amount)
	 */
	public static String messageStringGenerator(String mIdString, String mRandomIdString, String mAmountString) {

		String idBinaryString = idBinaryStringGenerator(mIdString);
		String randomIdBinaryString = randomIdBinaryStringGenerator(mRandomIdString);
		String amountBinaryString = amountBinaryStringGenerator(mAmountString);

		//		System.out.println("ID Binary String: " + idBinaryString);
		//		System.out.println("Amount Binary String: " + amountBinaryString);

		return idBinaryString + randomIdBinaryString + amountBinaryString;

	}

	/**
	 * This function generates id binary string with specified length
	 * 
	 * @param mIdString
	 * @return ID binary string with specified length
	 */
	public static String idBinaryStringGenerator(String mIdString) {

		String idBinaryString = 
				Long.toBinaryString(Long.valueOf(mIdString));

		for (int i = idBinaryString.length(); i < getIdNumberOfBits(); i++) {
			idBinaryString = "0" + idBinaryString;
		}

		return idBinaryString;
	}

	/**
	 * This function generates random id binary string with specified length
	 * 
	 * @param mRandomIdString
	 * @return random ID binary string with specified length
	 */
	public static String randomIdBinaryStringGenerator(String mRandomIdString) {

		String randomIdBinaryString = 
				Long.toBinaryString(Long.valueOf(mRandomIdString));

		for (int i = randomIdBinaryString.length(); i < getRandomIdNumberOfBits(); i++) {
			randomIdBinaryString = "0" + randomIdBinaryString;
		}

		return randomIdBinaryString;
	}
	
	/**
	 * This function generates amount binary string with specified length
	 * 
	 * @param mAmountString
	 * @return amount binary string with specified length
	 */
	public static String amountBinaryStringGenerator(String mAmountString) {

		String amountBinaryString =
				Integer.toBinaryString(Integer.valueOf(mAmountString) / getAmountReduceFactor());

		for (int i = amountBinaryString.length(); i < getReducedAmountNumberOfBits(); i++) {
			amountBinaryString = "0" + amountBinaryString;
		}

		return amountBinaryString;
	}

	/**
	 * This function reads the systematic generator matrix from file and generates
	 * the corresponding matrix (NxK).
	 * 
	 * @return systematic generator matrix
	 */
	public static Matrix readSystematicGeneratorMatrixFromFile() {

		int[] codingDimensions = parseSystematicGeneratroMatrixSizeFile();
		int N = codingDimensions[0];
		int K = codingDimensions[1];
		//		System.out.println(N + " x " + K);

		double[][] systematicGeneratorDouble = new double[N][K];

		try {

			int rowNumber = 0;
			for (String line : Files.readAllLines(Paths.get(getSystematicGeneratorMatrixFileName()))) {
				int colNumber = 0;
				for (String part : line.split("\\s+")) {
					int tempInt = Integer.valueOf(part);
					systematicGeneratorDouble[rowNumber][colNumber] = tempInt;
					colNumber++;
				}
				rowNumber++;
			}
		} catch (IOException e) {

			e.printStackTrace();
		}

		return new Matrix(systematicGeneratorDouble);
	}

	/**
	 * This functions parses the systematic generator matrix size file to specify
	 * N, K, and M. (M is not used in this context)
	 * N: length of code word
	 * K: length of message
	 * 
	 * @return array containing N, K, and M
	 */
	public static int[] parseSystematicGeneratroMatrixSizeFile() {

		int[] codingDimensions = new int[3]; // {N, K, M}
		try {
			int lineNumber = 1;
			for (String line : Files.readAllLines(Paths.get(getSystematicGeneratorMatrixSizeFileName()))) {

				int tempInt = Integer.valueOf(line);
				codingDimensions[lineNumber - 1] = tempInt;
				lineNumber++;
			}
			codingDimensions[2] = codingDimensions[0] - codingDimensions[1];
		} catch (IOException e) {

			e.printStackTrace();
		}
		return codingDimensions;
	}

	/**
	 * This function generates the message matrix from the message binary string
	 * 
	 * @param mMessageBinaryString
	 * @param mK
	 * @return message matrix
	 */
	public static Matrix generateMessageMatrix(String mMessageBinaryString, int mK) {

		double[][] messageDouble = new double[mK][1];

		for (int i = 0; i < mK; i++) {
			if (i < mK - mMessageBinaryString.length()) {
				messageDouble[i][0] = 0;
			} else {
				messageDouble[i][0] =
						Integer.parseInt(String.valueOf(mMessageBinaryString.charAt(i - (mK - mMessageBinaryString.length()))));
			}
		}

		return new Matrix(messageDouble);
	}

	/**
	 * This function generates the code word from the systematic generator matrix and
	 * message matrix (CW = G x M)
	 * 
	 * @param mSystematicGeneratorMatrix
	 * @param mMessageMatrix
	 * @return code word matrix
	 */
	public static Matrix generateCodeWordMatrix(Matrix mSystematicGeneratorMatrix, Matrix mMessageMatrix) {

		Matrix codeWordMatrix = mSystematicGeneratorMatrix.times(mMessageMatrix);

		for (int i = 0; i < codeWordMatrix.getRowDimension(); i++) {
			for (int j = 0; j < codeWordMatrix.getColumnDimension(); j++) {
				codeWordMatrix.set(i, j, ((int) codeWordMatrix.get(i, j)) % 2); 
			}
		}

		return codeWordMatrix;
	}

	/**
	 * This function generates sound samples of data (without signaling)
	 * based on code word matrix
	 * 
	 * @param mCodeWordMatrix
	 * @return array of sound samples
	 */
	public static double[] generateDataSoundSamplesFromCodeWordMatrix(Matrix mCodeWordMatrix) {

		double dataFrequencies[] = ChannelCodecUtilities.getDataFrequencies();

		double durationPerBit = ChannelCodecUtilities.getDurationPerBit();
		int sampleRate = ChannelCodecUtilities.getSampleRate();
		int numOfSamplesPerBit = (int) (durationPerBit * sampleRate);

		int numOfSamples = mCodeWordMatrix.getRowDimension() * numOfSamplesPerBit;

		double codeWordSoundSamples[] = new double[numOfSamples];

		int frequencyIndex = 0;

		for (int i = 0; i < mCodeWordMatrix.getRowDimension(); i++) {
			switch ((int) mCodeWordMatrix.get(i, 0)) {
			case 0:
				frequencyIndex = (frequencyIndex + 1) % 3;
				break;
			case 1:
				frequencyIndex = (frequencyIndex + 2) % 3;
				break;
			}

			for (int j = 0; j < numOfSamplesPerBit; j++) {
				codeWordSoundSamples[i * numOfSamplesPerBit + j] =
						Math.sin(2 * Math.PI * j / (sampleRate / dataFrequencies[frequencyIndex]));
			}
		}

		return codeWordSoundSamples;
	}

	/**
	 * This function generates sound samples to be transmitted (including signaling)
	 * 
	 * @param mCodeWordSoundSamples
	 * @return sound samples to be transmitted
	 */
	public static double[] generateSoundSamples(double[] mCodeWordSoundSamples) {

		int sampleRate = ChannelCodecUtilities.getSampleRate();

		int numOfSignalingSamples =
				(int) (ChannelCodecUtilities.getSignalingDuration() * sampleRate);

		double[] signalingSamples = new double[numOfSignalingSamples];

		for (int i = 0; i < numOfSignalingSamples; ++i) {
			signalingSamples[i] =
					Math.sin(2 * Math.PI * i / (sampleRate / ChannelCodecUtilities.getSignalingFrequency()));
		}

		double[] soundSamples = new double[signalingSamples.length +
		                                   mCodeWordSoundSamples.length +
		                                   signalingSamples.length];

		System.arraycopy(signalingSamples, 0, soundSamples,
				0, signalingSamples.length);

		System.arraycopy(mCodeWordSoundSamples, 0, soundSamples,
				signalingSamples.length, mCodeWordSoundSamples.length);

		System.arraycopy(signalingSamples, 0, soundSamples,
				signalingSamples.length + mCodeWordSoundSamples.length, signalingSamples.length);

		return soundSamples;

	}

	/**
	 * This function generates sound from sound samples
	 * 
	 * @param mSoundSamples
	 * @return sound generated from sound samples
	 */
	public static byte[] generateSoundFromSoundSamples(double[] mSoundSamples) {

		byte sound[] = new byte[2 * mSoundSamples.length];

		int index = 0;
		for (final double dVal : mSoundSamples) {

			// scale to maximum amplitude
			final short val = (short) ((dVal * 32767));

			// in 16 bit wav PCM, first byte is the low order byte
			sound[index++] = (byte) (val & 0x00ff);
			sound[index++] = (byte) ((val & 0xff00) >>> 8);
		}

		return sound;
	}

	/**
	 * This function writes sound data to 16 bit PCM wav file
	 * 
	 * @param mSound
	 */
	public static void saveSoundFile(byte[] mSound, String mTransmitFileName) {

		int numberOfChannels = 1;
		int bitsPerSample = 16;
		long soundDataSize = mSound.length;

		long chunkSize = 36 + soundDataSize * numberOfChannels * bitsPerSample / 8;

		long subChunk1Size = 16;
		int audioFormat = 1;

		int sampleRate = ChannelCodecUtilities.getSampleRate();
		long byteRate = sampleRate * numberOfChannels * bitsPerSample / 8;

		int blockAlign = numberOfChannels * bitsPerSample / 8;

		try {
			OutputStream os;
			os = new FileOutputStream(new File(mTransmitFileName));
			BufferedOutputStream bos = new BufferedOutputStream(os);
			DataOutputStream outFile = new DataOutputStream(bos);

			outFile.writeBytes("RIFF");                 // 00 - RIFF
			outFile.write(intToByteArray((int) chunkSize), 0, 4);     // 04 - how big is the rest of this file?
			outFile.writeBytes("WAVE");                 // 08 - WAVE
			outFile.writeBytes("fmt ");                 // 12 - fmt
			outFile.write(intToByteArray((int) subChunk1Size), 0, 4); // 16 - size of this chunk
			outFile.write(shortToByteArray((short) audioFormat), 0, 2);        // 20 - what is the audio format? 1 for PCM = Pulse Code Modulation
			outFile.write(shortToByteArray((short) numberOfChannels), 0, 2);  // 22 - mono or stereo? 1 or 2?  (or 5 or ???)
			outFile.write(intToByteArray((int) sampleRate), 0, 4);        // 24 - samples per second (numbers per second)
			outFile.write(intToByteArray((int) byteRate), 0, 4);      // 28 - bytes per second
			outFile.write(shortToByteArray((short) blockAlign), 0, 2);    // 32 - # of bytes in one sample, for all channels
			outFile.write(shortToByteArray((short) bitsPerSample), 0, 2); // 34 - how many bits in a sample(number)?  usually 16 or 24
			outFile.writeBytes("data");                 // 36 - data
			outFile.write(intToByteArray((int) soundDataSize), 0, 4);      // 40 - how big is this data chunk
			outFile.write(mSound);

			//			System.out.println("Chunk Size: " + chunkSize);
			//	        System.out.println("subChunk1 Size: " + subChunk1Size);
			//	        System.out.println("Audio format: " + audioFormat);
			//	        System.out.println("Number of channels: " + numberOfChannels);
			//	        System.out.println("Sample rate: " + sampleRate);
			//	        System.out.println("Byte rate: " + byteRate);
			//	        System.out.println("Block align: " + blockAlign);
			//	        System.out.println("Data Size: " + soundDataSize);

			outFile.flush();
			outFile.close();

		} catch (Exception e) {
			e.printStackTrace();;
		}
	}

	/**
	 * This function converts an integer to a byte array
	 * 
	 * @param mInt
	 * @return equal byte array
	 */
	private static byte[] intToByteArray(int mInt)	{

		byte[] byteArray = new byte[4];
		byteArray[0] = (byte) (mInt & 0x00FF);
		byteArray[1] = (byte) ((mInt>> 8) & 0x000000FF);
		byteArray[2] = (byte) ((mInt >> 16) & 0x000000FF);
		byteArray[3] = (byte) ((mInt >> 24) & 0x000000FF);
		return byteArray;
	}

	/**
	 * This function converts a short to a byte array
	 * 
	 * @param data
	 * @return equal byte array
	 */
	public static byte[] shortToByteArray(short data) {

		return new byte[]{(byte) (data & 0xff), (byte) ((data >>> 8) & 0xff)};
	}

	public static void main(String[] args) {

		long startTime = System.nanoTime();

		String idString = args[0];
		String randomIdString = args[1];
		String amountString = args[2];

		String messageBinaryString = messageStringGenerator(idString, randomIdString, amountString);
		//		System.out.println("Message: " + messageBinaryString);
		//		System.out.println("Message Length: " + messageBinaryString.length());

		Matrix systematicGeneratorMatrix = readSystematicGeneratorMatrixFromFile();

		Matrix messageMatrix = generateMessageMatrix(messageBinaryString, systematicGeneratorMatrix.getColumnDimension());
		//		System.out.println("Message:");
		//		messageMatrix.transpose().print(0, 0);

		Matrix codeWordMatrix = generateCodeWordMatrix(systematicGeneratorMatrix, messageMatrix);
		//		System.out.println("Code Word:");
		//		codeWordMatrix.transpose().print(0, 0);

		double[] codeWordSoundSamples = generateDataSoundSamplesFromCodeWordMatrix(codeWordMatrix);

		double[] soundSamples = generateSoundSamples(codeWordSoundSamples);

		byte[] sound = generateSoundFromSoundSamples(soundSamples);

		String transmitFileName = generateTransmitFileName(idString);

		saveSoundFile(sound, transmitFileName);

		System.out.println(transmitFileName.substring(getTransmitFilePath().length()));

		long runTime = System.nanoTime() - startTime;
		// System.out.println("Run Time: " + Long.toString(runTime) + (runTime == 1 ? " Nanosecond" : " Nanoseconds"));

	}
}
