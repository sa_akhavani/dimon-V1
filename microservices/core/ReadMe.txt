Compile command: 
	javac -cp lib/Jama-1.0.3.jar:lib/jfftpack.jar:lib/json-smart-1.1.1.jar ChannelCodecUtilities.java Encoder.java Decoder.java
	
Encoder run command:
	java -cp .:lib/Jama-1.0.3.jar Encoder {Passenger ID} {random} {amount}

Decoder terminal command:
	java -cp .:lib/Jama-1.0.3.jar:lib/jfftpack.jar:lib/json-smart-1.1.1.jar Decoder {file name (including path)} {Driver ID}



Instructions:

	1- Encoder generates a sound file and saves the file in "transmit" folder.
	2- Encoder uses this naming structure:
		"transmit" + "_" + {Passenger ID} + "_" + {Date (ddMMyy)} + "_" + {Time (HHmmss)} + "." + {file extension}
	3- Decoder reads sound file from {file name (including path)}.
	4- Decoder reads sound file using this naming structure:
		"receive" + "_" + {Driver ID} + "." + {file extension}
	5- Decoder renames the file using this naming structure:
		"receive" + "_" + {Driver ID} + "_" + {Date (ddMMyy)} + "_" + {Time (HHmmss)} + "." + {file extension}
	6- Decoder returns JSON with following structure (id is the passenger's id):
		{
			amount: (int)
			rnd: (int)
			id: (String)
		}