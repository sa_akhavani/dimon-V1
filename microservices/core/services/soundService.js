var exec = require('child_process').exec;

var core = {};

core.generateSound = function (id, rnd, amount,callback) {
	// var command = 'java -cp .:lib/Jama-1.0.3.jar Encoder ' + id.toString() + ' ' + rnd.toString() + ' ' + amount;
	var command = 'java -cp .:lib/Jama-1.0.3.jar Encoder 5 111 1300';
	exec(command, function(error, stdout, stderr) {
		exec(command, function(error, stdout, stderr) {
			if (error) callback(error);
			else {
				return callback();
			}
		});
	});
}

core.decodeSound = function() {
	var command = 'java -cp .:lib/Jama-1.0.3.jar:lib/jfftpack.jar:lib/json-smart-1.1.1.jar Decoder';
	exec(command, function(error, stdout, stderr) {
		if (error) return -1;
		else {
			var stdObj = JSON.parse(stdout);
			return stdObj;
		}
	});
}

module.exports = core;