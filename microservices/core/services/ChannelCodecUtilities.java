import Jama.Matrix;

public class ChannelCodecUtilities {

	// Block size used in FFT
	static int blockSize = 256;

	// Sample rate
	static int sampleRate = 44100;
	
	// Three frequencies used in data transfer
    private static double frequencyLow     = 19000;
    private static double frequencyMid     = 19500;
    private static double frequencyHigh    = 20000;

    // Frequency used for signaling data transfer start and end points
    private static double signalingFrequency = 20500;

 // Duration of each bit
 	private static double durationPerBit = 0.01;
 	
 	// Duration of signaling (start, end, and between data transfers)
     private static double signalingDuration = 0.2;
    
	// Threshold to detect ultrasound frequencies
	private static double ultraSoundFrequencyThreshold = 18000;

	// Threshold for detecting sound
	private static double volumeThreshold = 0.3;

	public static int getBlockSize() {
		return blockSize;
	}

	public static int getSampleRate() {
		return sampleRate;
	}

	public static double getUltraSoundFrequencyThreshold() {
		return ultraSoundFrequencyThreshold;
	}
	
	public static double getVolumeThreshold() {
		return volumeThreshold;
	}
	
	public static double[] getDataFrequencies() {
        double dataFrequencies[] = {frequencyLow, frequencyMid, frequencyHigh};
        return dataFrequencies;
    }
	
	public static double getSignalingFrequency() {
        return signalingFrequency;
    }
	
	public static double getSignalingDuration() {
		return signalingDuration;
	}
	
	public static double getDurationPerBit() {
        return durationPerBit;
    }
	
	/**
	 * This function generates code word matrix from a binary string
	 * 
	 * @param mBinaryString
	 * @param mN
	 * @return code word matrix
	 */
	public static Matrix generateCodeWordMatrixFromBinaryString(String mBinaryString, int mN) {

        double[][] codeWordDouble = new double[mN][1];
        if (mBinaryString.length() < mN) {
            while (mBinaryString.length() != mN) {
                mBinaryString += "0";
            }
        }
        for (int i = 0; i < mBinaryString.length(); i++) {
            codeWordDouble[i][0] = (mBinaryString.charAt(i) == '0') ? 0 : 1;
        }

        return new Matrix(codeWordDouble);
    }
	
	/**
	 * This function decodes the code word matrix and does error correction using iterative decoding
	 * 
	 * @param mParityCheckMatrix
	 * @param mCodeWordMatrix
	 * @param mN
	 * @param mM
	 * @return corrected code word matrix
	 */
	public static Matrix iterativeDecoding(Matrix mParityCheckMatrix, Matrix mCodeWordMatrix, int mN, int mM) {

        double initialProbability = 0.8;
        //		System.out.println("One Probability: " + (1 / (1 + Math.exp(-2 * initialProbability))));
        int maxIterations = 30;

        Matrix Pn1 = new Matrix(mN, 1);
        Matrix Pn0 = new Matrix(mN, 1);

        for (int i = 0; i < mCodeWordMatrix.getRowDimension(); i++) {
            if (mCodeWordMatrix.get(i, 0) == 1) {
                Pn1.set(i, 0, 1 / (1 + Math.exp(-2 * initialProbability)));
                Pn0.set(i, 0, 1 - 1 / (1 + Math.exp(-2 * initialProbability)));
            } else {
                Pn0.set(i, 0, 1 / (1 + Math.exp(-2 * initialProbability)));
                Pn1.set(i, 0, 1 - 1 / (1 + Math.exp(-2 * initialProbability)));
            }
        }

        Matrix qmn0 = new Matrix(mM, mN);
        Matrix qmn1 = new Matrix(mM, mN);

        for (int i = 0; i < mN; i++) {
            qmn0.setMatrix(0, mM - 1, i, i, Pn0);
            qmn1.setMatrix(0, mM - 1, i, i, Pn1);
        }
        qmn1.arrayTimesEquals(mParityCheckMatrix);
        qmn0.arrayTimesEquals(mParityCheckMatrix);

        Matrix qn0 = new Matrix(mN, 1);
        Matrix qn1 = new Matrix(mN, 1);

        Matrix rmn1 = new Matrix(mM, mN);
        Matrix rmn0 = new Matrix(mM, mN);

        Matrix deltaR = new Matrix(mM, mN);
        Matrix deltaQ = new Matrix(mM, mN);

        Matrix Cn = new Matrix(mN, 1);
        Matrix Zm = new Matrix(mM, 1);

        boolean isFinished = false;
        int iterationNumber = 1;

        while (!isFinished) {

            /////////////////////////////////////////////////////////////////////////////////////////////
            deltaQ = qmn0.minus(qmn1);

            for (int i = 0; i < mM; i++) {
                for (int j = 0; j < mN; j++) {

                    if (mParityCheckMatrix.get(i, j) == 1) {
                        double multiplicationTemp = 1;
                        for(int k = 0; k < mN; k++) {
                            if ((k != j) && (mParityCheckMatrix.get(i, k) == 1)) {
                                multiplicationTemp *= deltaQ.get(i, k);
                            }
                        }
                        deltaR.set(i, j, multiplicationTemp);
                    }

                }
            }

            for (int i = 0; i < mM; i++) {
                for (int j = 0; j < mN; j++) {
                    if (mParityCheckMatrix.get(i, j) == 1) {
                        rmn1.set(i, j, (1 - deltaR.get(i, j)) / 2);
                        rmn0.set(i, j, (1 + deltaR.get(i, j)) / 2);
                    }
                }
            }

            /////////////////////////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < mM; i++) {
                for (int j = 0; j < mN; j++) {
                    if(mParityCheckMatrix.get(i, j) == 1) {
                        double multiplicationTemp0 = Pn0.get(j, 0);
                        double multiplicationTemp1 = Pn1.get(j, 0);
                        for (int k = 0; k < mM; k++) {
                            if ((k != i) && (mParityCheckMatrix.get(k, j) == 1)) {
                                multiplicationTemp0 *= rmn0.get(k, j);
                                multiplicationTemp1 *= rmn1.get(k, j);
                            }
                        }

                        double alphamn = 1 / (multiplicationTemp0 + multiplicationTemp1);

                        qmn0.set(i, j, alphamn * multiplicationTemp0);
                        qmn1.set(i, j, alphamn * multiplicationTemp1);
                    }
                }
            }

            /////////////////////////////////////////////////////////////////////////////////////////////

            for (int i = 0; i < mN; i++) {
                double multiplicationTemp0 = Pn0.get(i, 0);
                double multiplicationTemp1 = Pn1.get(i, 0);
                for (int j = 0; j < mM; j++) {
                    if ((j != i) && (mParityCheckMatrix.get(j, i) == 1)) {
                        multiplicationTemp0 *= rmn0.get(j, i);
                        multiplicationTemp1 *= rmn1.get(j, i);
                    }
                }

                double alphan = 1 / (multiplicationTemp0 + multiplicationTemp1);

                qn0.set(i, 0, alphan * multiplicationTemp0);
                qn1.set(i, 0, alphan * multiplicationTemp1);

            }

            /////////////////////////////////////////////////////////////////////////////////////////////

            for (int i = 0; i < mN; i++) {
                if (qn1.get(i, 0) > 0.5) {
                    Cn.set(i, 0, 1);
                } else {
                    Cn.set(i, 0, 0);
                }
            }

            Zm = mParityCheckMatrix.times(Cn);
            for (int i = 0; i < mM; i++) {
                Zm.set(i, 0, Zm.get(i, 0) % 2);
            }

            boolean isNotEqual = false;

            for (int i = 0; i < mM; i++) {
                if (Zm.get(i, 0) == 1) {
                    isNotEqual = true;
                    break;
                }
            }

            if ((!isNotEqual) || (iterationNumber >= maxIterations)) {
                isFinished = true;
                if((!isNotEqual)) {
                    // System.out.println("Decoding finished in " + iterationNumber + (iterationNumber == 1 ? " iteration" : " iterations"));
                    return Cn;
                } else {
                    // System.out.println("Decoding not finished");
                    return null;
                }
            }
            iterationNumber++;
        }

        return null;
    }

	/**
	 * This function generates message matrix from code word matrix
	 * 
	 * @param mCodeWordMatrix
	 * @param mK
	 * @param mM
	 * @param mN
	 * @return message matrix
	 */
	public static Matrix generateMessageMatrixFromCodeWordMatrix(Matrix mCodeWordMatrix, int mK, int mM, int mN) {

        double[][] messageDouble = new double[mK][1];
        for (int i = mM; i < mN; i++) {
            messageDouble[i - mM][0] = mCodeWordMatrix.get(i, 0);
        }

        return new Matrix(messageDouble);
    }
	
	/**
	 * This function generates binary string from binary matrix
	 * 
	 * @param mBinaryMatrix
	 * @return binary string
	 */
	public static String generateBinaryStringFromBinaryMatrix(Matrix mBinaryMatrix) {

        String binaryString = "";
        for (int i = 0; i < mBinaryMatrix.getRowDimension(); i++) {
            binaryString += Integer.toString((int) mBinaryMatrix.get(i, 0));
        }

        return binaryString;
    }
	
}
