Compile command: 
	javac -cp lib/Jama-1.0.3.jar:lib/jfftpack.jar:lib/json-smart-1.1.1.jar ChannelCodecUtilities.java Encoder.java Decoder.java
	
Encoder run command:
	java -cp .:lib/Jama-1.0.3.jar Encoder {ID} {random} {amount}

Decoder terminal command:
	java -cp .:lib/Jama-1.0.3.jar:lib/jfftpack.jar:lib/json-smart-1.1.1.jar Decoder