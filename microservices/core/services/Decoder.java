import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;

import FFT.RealDoubleFFT;
import Jama.Matrix;
import net.minidev.json.JSONObject;

public class Decoder {

	
	static String inFileName = "transmit_sound.wav";

	// Name of file containing generator matrix size
	static String systematicGeneratorMatrixSizeFileName = "systematicGeneratorMatrixSize.txt";
	// Name of file containing generator matrix
	static String systematicGeneratorMatrixFileName = "systematicGeneratorMatrix.txt";
	// Parity check matrix file name
	private static String parityCheckMatrixFileName = "systematicParityCheckMatrix.txt";

	static int amountReduceFactor = 10; // Factor to divide amount by

	static int idNumberOfBits 				= 32; // Number of bits used to show id of users
	static int randomIdNumberOfBits			= 8;  // Number of bits used to show the random id generated for this transaction
	static int reducedAmountNumberOfBits 	= 16; // Number of bits used to show the reduced amount (amount / amountReduceFactor)

	/**
	 * Input file name getter
	 * 
	 * @return input file name
	 */
	public static String getInFileName() {
		return inFileName;
	}

	/**
	 * Systematic generator matrix size file name getter
	 * 
	 * @return systematic generator matrix size file name
	 */
	public static String getSystematicGeneratorMatrixSizeFileName() {
		return systematicGeneratorMatrixSizeFileName;
	}

	public static String getSystematicGeneratorMatrixFileName() {
		return systematicGeneratorMatrixFileName;
	}

	public static String getParityCheckMatrixFileName() {
		return parityCheckMatrixFileName;
	}

	public static int getAmountReduceFactor() {
		return amountReduceFactor;
	}

	public static int getIdNumberOfBits() {
		return idNumberOfBits;
	}

	public static int getRandomIdNumberOfBits() {
		return randomIdNumberOfBits;
	}
	
	public static int getReducedAmountNumberOfBits() {
		return reducedAmountNumberOfBits;
	}

	/**
	 * This functions parses the systematic generator matrix size file to specify
	 * N, K, and M. (M is not used in this context)
	 * N: length of code word
	 * K: length of message
	 * 
	 * @return array containing N, K, and M
	 */
	public static int[] parseSystematicGeneratroMatrixSizeFile() {

		int[] codingDimensions = new int[3]; // {N, K, M}
		try {
			int lineNumber = 1;
			for (String line : Files.readAllLines(Paths.get("misc/" + getSystematicGeneratorMatrixSizeFileName()))) {

				int tempInt = Integer.valueOf(line);
				codingDimensions[lineNumber - 1] = tempInt;
				lineNumber++;
			}
			codingDimensions[2] = codingDimensions[0] - codingDimensions[1];
		} catch (IOException e) {

			e.printStackTrace();
		}
		return codingDimensions;
	}

	/**
	 * This function reads the systematic generator matrix from file and generates
	 * the corresponding matrix (NxK).
	 * 
	 * @return systematic generator matrix
	 */
	public static Matrix readParityCheckMatrixFromFile() {

		int[] codingDimensions = parseSystematicGeneratroMatrixSizeFile();
		int N = codingDimensions[0];
		int M = codingDimensions[2];

		double[][] parityCheckDouble = new double[M][N];

		try {

			int rowNumber = 0;
			for (String line : Files.readAllLines(Paths.get("misc/" + getParityCheckMatrixFileName()))) {
				int colNumber = 0;
				for (String part : line.split("\\s+")) {
					int tempInt = Integer.valueOf(part);
					parityCheckDouble[rowNumber][colNumber] = tempInt;
					colNumber++;
				}
				rowNumber++;
			}
		} catch (IOException e) {

			e.printStackTrace();
		}

		return new Matrix(parityCheckDouble);
	}

	/**
	 * This function reads the recorded wav file and turns it into a byte sequence 
	 * 
	 * @return byte array of sound of data
	 */
	public static byte[] readSoundFile() {
		DataInputStream inFile = null;
		byte[] sound = null;
		byte[] tmpLong = new byte[4];
		byte[] tmpInt = new byte[2];

		try {
			inFile = new DataInputStream(new FileInputStream("sound/" + getInFileName()));

			String chunkID = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();

			inFile.read(tmpLong); // read the chunk size
			long chunkSize = byteArrayToLong(tmpLong);

			String format = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();

			String subChunk1ID = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();

			inFile.read(tmpLong); // read the SubChunk1 Size
			long subChunk1Size = byteArrayToLong(tmpLong);

			inFile.read(tmpInt); // read the audio format.  This should be 1 for PCM
			int audioFormat = (int) byteArrayToLong(tmpInt);

			inFile.read(tmpInt); // read # of channels (1 or 2)
			int numberOfChannels = (int) byteArrayToLong(tmpInt);

			inFile.read(tmpLong); // read the sample rate
			int sampleRate = (int) byteArrayToLong(tmpLong);

			inFile.read(tmpLong); // read the byte rate
			long byteRate = byteArrayToLong(tmpLong);

			inFile.read(tmpInt); // read the block align
			int blockAlign = (int) byteArrayToLong(tmpInt);

			inFile.read(tmpInt); // read the bits per sample
			int bitsPerSample = (int) byteArrayToLong(tmpInt);

			// read the data chunk header - reading this IS necessary, because not all wav files will have the data chunk here - for now, we're just assuming that the data chunk is here
			String dataChunkID = "" + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte() + (char) inFile.readByte();

			inFile.read(tmpLong); // read the size of the data
			long soundDataSize = byteArrayToLong(tmpLong);

			// read the data chunk
			sound = new byte[(int) soundDataSize];
			inFile.read(sound);

			// close the input stream
			inFile.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return sound; // this should probably be something more descriptive
	}

	/**
	 * This function converts a byte array to a long
	 * 
	 * @param mB
	 * @return equal long
	 */
	public static long byteArrayToLong(byte[] mB) {

		long longValue = 0;
		for (int i = 0; i < mB.length; i++) {
			longValue += ((long) mB[i] & 0xffL) << (8 * i);
		}

		return longValue;
	}

	/**
	 * This function decodes the sound file
	 * 
	 * @param mSound
	 */
	public static void decodeSound(byte[] mSound) {

		int blockSize = ChannelCodecUtilities.getBlockSize();

		RealDoubleFFT transformer = new RealDoubleFFT(blockSize);

		boolean isDecoding = true;
		boolean isSignalDetected = false;
		boolean isDataStarted = false;
		int blockCount = 0;

		double[] recordedFrequencies = calculateFrequencies();

		double ultraSoundFrequencyThreshold    = ChannelCodecUtilities.getUltraSoundFrequencyThreshold();
		double volumeThreshold                 = ChannelCodecUtilities.getVolumeThreshold();

		double dataFrequencies[]    = ChannelCodecUtilities.getDataFrequencies();
		double signalingFrequency	= ChannelCodecUtilities.getSignalingFrequency();

		int numOfSignalingsDetected = 0;
		double previousFrequency = 0;

		String receiveString = "";

		short[] soundSamples = new short[mSound.length / 2];

		ByteBuffer.wrap(mSound).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(soundSamples);

		while(isDecoding) {

			double[] toTransform;
			if ((blockCount + 1) * blockSize < soundSamples.length) {
				toTransform = new double[blockSize];
			} else {
				continue;
			}

			for (int i = 0; i < toTransform.length; i++) {
				toTransform[i] = (double) soundSamples[i + blockCount * blockSize] / 32768.0;
			}	

			transformer.ft(toTransform);

			int maxAmplitudeIndex = findMaxAmplitudeIndex(toTransform, recordedFrequencies,
					ultraSoundFrequencyThreshold, volumeThreshold);

			if (maxAmplitudeIndex != 0) {
				double maxFrequency = recordedFrequencies[maxAmplitudeIndex];
				if (!isSignalDetected) {
					double peakFrequency = findPeakFrequency(maxFrequency);
					if (peakFrequency == signalingFrequency) {
						isSignalDetected = true;
						numOfSignalingsDetected++;
						previousFrequency = peakFrequency;
					}
				} else {
					double peakFrequency = findPeakFrequency(maxFrequency);
					if (peakFrequency != previousFrequency) {
						if (peakFrequency != signalingFrequency) {
							if (!isDataStarted) {
								isDataStarted = true;
								previousFrequency = dataFrequencies[0];
							}
							receiveString += decodeBit(peakFrequency, previousFrequency);
							previousFrequency = peakFrequency;
						} else {
							numOfSignalingsDetected++;

							String codeWordBinaryString = receiveString;

							int[] codingDimensions = parseSystematicGeneratroMatrixSizeFile();
							int N = codingDimensions[0];
							int K = codingDimensions[1];
							int M = codingDimensions[2];

							Matrix parityCheckMatrix = readParityCheckMatrixFromFile();

							Matrix codeWordMatrix = 
									ChannelCodecUtilities.generateCodeWordMatrixFromBinaryString(codeWordBinaryString, N);

							Matrix detectedCodeWordMatrix = ChannelCodecUtilities.iterativeDecoding(parityCheckMatrix,
									codeWordMatrix, N, M);

							Matrix detectedMessageMatrix = ChannelCodecUtilities.generateMessageMatrixFromCodeWordMatrix(detectedCodeWordMatrix, K, M, N);

							String detectedMessageBinaryString =
									ChannelCodecUtilities.generateBinaryStringFromBinaryMatrix(detectedMessageMatrix);

							String[] detectedMessageParts = messageStringSeparator(detectedMessageBinaryString);
							String idBinaryString = detectedMessageParts[0];
							String randomIdBinaryString = detectedMessageParts[1];
							String amoundBinaryString = detectedMessageParts[2];

							String idString = Long.toString(Long.parseLong(idBinaryString, 2));
							int randomId = Integer.parseInt(randomIdBinaryString, 2);
							int amount = Integer.parseInt(amoundBinaryString, 2) * getAmountReduceFactor();

							JSONObject outJSONObject = new JSONObject();
							outJSONObject.put("id", idString);
							outJSONObject.put("rnd", randomId);
							outJSONObject.put("amount", amount);

							System.out.println(outJSONObject);

							if (numOfSignalingsDetected >= 2) {
								isDecoding = false;
							}
							previousFrequency = signalingFrequency;
							receiveString = "";
						}
					}
				}
			}
			blockCount++;
		}
	}

	/**
	 * This function decodes one bit based on the difference between peak frequency and previous frequency
	 * 
	 * @param mPeakFrequency
	 * @param mPreviousFrequency
	 * @return 0 or 1
	 */
	private static String decodeBit(double mPeakFrequency, double mPreviousFrequency) {
		
		String bitString = "";
		
		if (mPeakFrequency > mPreviousFrequency) {
			if (Math.abs(mPeakFrequency - mPreviousFrequency) == 500) {
				bitString = "0";
			} else {
				bitString = "1";
			}
		} else if (mPeakFrequency < mPreviousFrequency) {
			if (Math.abs(mPeakFrequency - mPreviousFrequency) == 500) {
				bitString = "1";
			} else {
				bitString = "0";
			}
		}
		return bitString;
	}
	
	/**
	 * This function separates different parts of the detected message binary string
	 * 
	 * @param mDetectedMessageBinaryString
	 * @return message parts string array {ID, random ID, amount} 
	 */
	private static String[] messageStringSeparator(String mDetectedMessageBinaryString) {

		String idBinaryString = mDetectedMessageBinaryString.substring(0, getIdNumberOfBits());
		String randomIdBinaryString = mDetectedMessageBinaryString.substring(getIdNumberOfBits(), getIdNumberOfBits() + getRandomIdNumberOfBits());
		String amountBinaryString = mDetectedMessageBinaryString.substring(getIdNumberOfBits() + getRandomIdNumberOfBits());

		String[] messageParts = {idBinaryString, randomIdBinaryString, amountBinaryString};

		return messageParts;
	}

	/**
	 * This function calculates all frequencies used in FFT based on sample rate and block size
	 * 
	 * @return array of frequencies used in FFT
	 */
	private static double[] calculateFrequencies() {

		int blockSize = ChannelCodecUtilities.getBlockSize();
		int sampleRate = ChannelCodecUtilities.getSampleRate(); 

		double[] recordedFrequencies = new double[blockSize];

		for (int i = 0; i < blockSize; i++) {
			recordedFrequencies[i] = (i + 1) * sampleRate / blockSize / 2;
		}

		return recordedFrequencies;
	}

	/**
	 * This function finds the index of the maximum amplitude
	 * 
	 * @param mToTransform
	 * @param mRecordedFrequencies
	 * @param mUltraSoundFrequencyThreshold
	 * @param mVolumeThreshold
	 * @return index of maximum amplitude
	 */
	private static int findMaxAmplitudeIndex(double[] mToTransform, double[] mRecordedFrequencies,
			double mUltraSoundFrequencyThreshold, double mVolumeThreshold) {

		double  maxAmplitude        = 0;
		int     maxAmplitudIndex    = 0;

		for (int i = 0; i < mToTransform.length; i++) {
			if (mRecordedFrequencies[i] >= mUltraSoundFrequencyThreshold) {
				if ((Math.abs(mToTransform[i]) > maxAmplitude) && (Math.abs(mToTransform[i]) > mVolumeThreshold)) {
					maxAmplitude = Math.abs(mToTransform[i]);
					maxAmplitudIndex = i;
				}
			}
		}

		return maxAmplitudIndex;
	}

	/**
	 * This function finds the peak frequency among reference frequencies
	 * 
	 * @param mMaxFrequency
	 * @return detected peak frequency
	 */
	private static double findPeakFrequency(double mMaxFrequency) {

		double dataFrequencies[]    = ChannelCodecUtilities.getDataFrequencies();
		double signalingFrequency	= ChannelCodecUtilities.getSignalingFrequency();

		double dist[] = {
				Math.abs(mMaxFrequency - dataFrequencies[0]),
				Math.abs(mMaxFrequency - dataFrequencies[1]),
				Math.abs(mMaxFrequency - dataFrequencies[2]),
				Math.abs(mMaxFrequency - signalingFrequency)};

		double peakFrequency = 0;

		if (dist[0] < dist[1] && dist[0] < dist[2] && dist[0] < dist[3]) {
			peakFrequency = dataFrequencies[0];
		} else if (dist[1] < dist[0] && dist[1] < dist[2] && dist[1] < dist[3]) {
			peakFrequency = dataFrequencies[1];
		} else if ((dist[2] < dist[0] && dist[2] < dist[1] && dist[2] < dist[3])){
			peakFrequency = dataFrequencies[2];
		} else if ((dist[3] < dist[0] && dist[3] < dist[1] && dist[3] < dist[2])){
			peakFrequency = signalingFrequency;
		}

		return peakFrequency;
	}

	public static void main(String[] args) {

		long startTime = System.nanoTime();

		byte[] sound = readSoundFile();

		decodeSound(sound);

		long runTime = System.nanoTime() - startTime;
		// System.out.println("Run Time: " + Long.toString(runTime) + (runTime == 1 ? " Nanosecond" : " Nanoseconds"));

	}

}
