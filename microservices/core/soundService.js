var exec = require('child_process').exec;

var core = {};

core.generateSound = function(id, rnd, amount, callback) {
	var command = 'java -cp .:lib/Jama-1.0.3.jar Encoder ' + id.toString() + ' ' + rnd.toString() + ' ' + amount;
	exec(command, function(error, stdout, stderr) {
		exec(command, function(error, stdout, stderr) {
			if (error) callback(error, null);
			else {
				var fileName = stdout.substring(0, stdout.length - 1);
				return callback(null, fileName);
			}
		});
	});
}

core.decodeSound = function(fileName, callback) {
	var command = 'java -cp .:lib/Jama-1.0.3.jar:lib/jfftpack.jar:lib/json-smart-1.1.1.jar Decoder ' +'./uploads/' + fileName + ' 1';

	exec(command, function(error, stdout, stderr) {
		if (error) callback(-1);
		else {
			var stdObj = JSON.parse(stdout);
			callback(stdObj);
		}
	});
}

module.exports = core;