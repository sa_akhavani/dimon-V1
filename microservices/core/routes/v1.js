var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var querystring = require('querystring');
var http = require('http');
var soundService = require('../soundService');
const redis = require("redis");
const client = redis.createClient();

const secretKey = 'sg92pjfslGq2r6jglkAH3fdSGlk2jRFfaSDF32F@#RSdfralaidjliLJW';


var paylist = [];
var successfulPayments = [];
var failedPayments = [];

function checkLastTransaction(token, callback) {
	var options = {
		host: 'localhost',
		port: 4000,
		path: '/v1/lasttransaction',
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'token': token
		}
	};
	var req = http.request(options, function(res) {
		res.setEncoding('utf8');
		res.on('data', (chunk) => {
			if (chunk.result != false) {
				var body = JSON.parse(chunk);
				if (body.result.lasttransaction != null) {
					callback(body.result);
				} else {
					callback(-1);
				}
			} else {
				callback(-1);
			}

		});
	});
	req.end();
}

function checkForSuccess(count, res, rid, token, soundObj) {
	if (count == 15) {
		checkLastTransaction(token, function(result) {
			if (result == -1) {
				return res.ok(false, "Payment not successful", 500);
			} else {
				var now = new Date();
				var lastupdate = new Date(result.lasttransaction.updatedAt);
				if (now - lastupdate < 60000) {
					return res.ok(true);
				} else {
					return res.ok(false, "Payment not successful", 500);
				}
			}
		})
	} else {
		setTimeout(function() {
			for (var i = 0; i < paylist.length; i++) {
				if (paylist[i].rid == rid && paylist[i].sid == soundObj.id && paylist[i].rnd == soundObj.rnd) {
					if (paylist[i].status == 3) {
						handleSuccessfulPayment(i);
						return res.ok(true);
					} else if (paylist[i].status == -1) {
						return res.ok(false, "Payment not successful", 500);
					} else if (paylist[i].status == 2){
						handleFailedPayment(i);
					}
				}
			}
			// print();
			checkForSuccess(count + 1, res, rid, token, soundObj);
		}, 1500)
	}
}


function checkForList(count, res, sid) {
	if (count == 15) {
		return res.ok(false, "empty", 404);
	} else {
		setTimeout(function() {
			var pay = [];
			for (var i = 0; i < paylist.length; i++) {
				if (paylist[i].sid == sid && paylist[i].status == 1) {
					paylist[i].status = 2;
					pay.push(paylist[i]);
					return res.ok(pay);
				}
			}
			checkForList(count + 1, res, sid);
		}, 1000);
	}
}

function updateAccountingDB(suuid, payment, callback) {
	var data = querystring.stringify({
		uuids: suuid,
		uuidr: payment.ruuid,
		amount: payment.amount
	});
	var options = {
		host: 'localhost',
		port: 4000, //???
		path: '/v1/updatebalance',
		method: 'PUT',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': Buffer.byteLength(data)
		}
	};

	var req = http.request(options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function(chunk) {
			if (res.statusCode == 200) {
				if (chunk.indexOf('err') < 0) {
					for (var i = 0; i < paylist.length; i++) {
						if (paylist[i].sid == payment.sid && paylist[i].rid == payment.rid && paylist[i].amount == payment.amount) {
							paylist[i].status = 3;
						}
					}
				} else {
					for (var i = 0; i < paylist.length; i++) {
						if (paylist[i].sid == payment.sid && paylist[i].rid == payment.rid && paylist[i].amount == payment.amount) {
							paylist[i].status = -1;
							handleFailedPayment(i);
						}
					}
					return callback("Low Credit");
				}
				return callback();
			} else {
				for (var i = 0; i < paylist.length; i++) {
					if (paylist[i].sid == payment.sid && paylist[i].rid == payment.rid && paylist[i].amount == payment.amount) {
						paylist[i].status = -1;
						handleFailedPayment(i);
					}
				}
				return callback("Bad connection");
			}
		});
	});
	req.write(data);
	req.end();
}


function handleSuccessfulPayment(i) {
	successfulPayments.push(paylist[i]);
	paylist.splice(i, 1);
}

function handleFailedPayment(i) {
	failedPayments.push(paylist[i]);
	paylist.splice(i, 1);
}

function ensureAuth(req, res, next) {
	var token = req.headers.token;
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			client.get(decoded.username, function(err, result) {
				if (decoded.uuid == result) {
					return next();
				} else {
					res.ok(false, 'Auth failed', 401);
				}
			});
		}
	});
}

function print() {
	console.log(paylist);
}

router.post('/getsound', ensureAuth, function(req, res, next) {
	var amount = req.body.amount;
	var token = req.headers.token;
	if (!token || !amount) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			var sid = decoded.id;
			var rnd = Math.floor(Math.random() * 255);
			soundService.generateSound(sid, rnd, amount, function(err, fileName) {
				if (err) return res.ok(false, err, 500);
				else {
					res.download(__dirname + '/../transmit/' + fileName, fileName + '.wav');
				}
			});
		}
	});
});



router.post('/sendsound', ensureAuth, function(req, res, next) {
	if (req.file != undefined) {
		var originalName = req.file.originalname;
		var fileName = req.file.filename;
		var mime = req.file.mimetype;
		var filepath = req.file.path;
		var ext = req.file.extension;
		var size = req.file.size;
	} else {
		return res.ok(false, "No file Uploaded", 404);
	}

	soundService.decodeSound(fileName, function(soundObj) {
		if (soundObj == -1) {
			return res.ok(false, "Java Code Error", 500);
		} else {
			var token = req.headers.token;
			if (!token) return res.ok(false, "Bad Request.", 400);
			jwt.verify(token, secretKey, function(err, decoded) {
				if (err) return res.ok(false, err, 502);
				else {
					var rid = decoded.id;
					var payment = {
						rid: rid,
						sid: parseInt(soundObj.id),
						rnd: soundObj.rnd,
						amount: soundObj.amount,
						rname: decoded.name,
						ruuid: decoded.uuid,
						status: 1
					}
					paylist.push(payment);
					print();
					var count = 0;
					checkForSuccess(count, res, rid, token, soundObj);

				}
			});
		}
	});
});



router.get('/getlist', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	if (!token) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			var sid = decoded.id;
			var count = 0;
			checkForList(count, res, sid);
		}
	});
});


router.post('/verifyreciver', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	if (!token) return res.ok(false, "Bad Request.", 400);

	// var payment = req.body;
	// var {
	// 	rid,
	// 	sid,
	// 	rnd,
	// 	amount,
	// 	rname,
	// 	ruuid
	// } = req.body

	jwt.verify(token, secretKey, function(err, decoded) {
		var suuid = decoded.uuid;
		updateAccountingDB(suuid, req.body, function(err) {
			if (err) res.ok(false, err, 500);
			else {
				res.ok(true);
			}
		});
	});
});

router.post('/upload', function(req, res, next) {
	if (req.file != undefined) {
		var originalName = req.file.originalname;
		var fileName = req.file.filename;
		var mime = req.file.mimetype;
		var filepath = req.file.path;
		var ext = req.file.extension;
		var size = req.file.size;
		res.send({
			originalName,
			fileName,
			mime,
			filepath,
			ext,
			size
		});

	} else {
		console.log("NO FILE UPLOADED");
	}

})

router.get('/list', function(req, res, next) {
	print();
	res.end();
});

module.exports = router;