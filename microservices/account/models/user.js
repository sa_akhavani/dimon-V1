var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var transactionSchema = new Schema({
	amount: {
		type: Number,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	date: {
		type: String,
		required: true
	}
}, {
	timestamps: true
});


var userSchema = new Schema({
	uuid: {
		type: String,
		required: true
	},
	balance: {
		type: Number,
		required: true
	},
	transactions: [transactionSchema]
}, {
	timestamps: true
});

var Users = mongoose.model('User', userSchema);
module.exports = Users;

module.exports.addUser = function(newUser, callback) {
	newUser.save(callback);
}

module.exports.raiseBalance = function(uuid, amount, callback) {
	if (!uuid) callback('No id provided.');
	Users.findOne({
		uuid: uuid
	}, function(err, user) {
		if (err) callback(err);
		if (!user) callback('User not found.');
		else {
			user.balance = user.balance + parseInt(amount);
			var newTransaction = {
				amount: amount,
				description: "افزایش اعتبار",
				date: new Date()
			}
			user.transactions.push(newTransaction);
			user.save(callback);
		}
	});
}

module.exports.getBalance = function(uuid, callback) {
	if (!uuid) callback('No id provided.', null);
	Users.findOne({
		uuid: uuid
	}, function(err, user) {
		if (err) callback(err);
		if (!user) callback('User not found.', null);
		else {
			callback(null, user.balance);
		}
	});
}

module.exports.getHistory = function(uuid, number, callback) {
	if (!uuid) callback('No id provided.', null);
	Users.findOne({
		uuid: uuid
	}, function(err, user) {
		if (err) callback(err);
		if (!user) callback('User not found.', null);
		else {
			if (number == null)
				return callback(null, user.transactions);
			else {
				var transactions = [];
				// if(number>user.transactions.length){
				// 	number=user.transactions.length;
				// }
				for (var i = 0; i < number; i++) {
					transactions.push(user.transactions[user.transactions.length - 1 - i]);
				}
				return callback(null, transactions);
			}
		}
	});
}

module.exports.getLastTransaction = function(uuid, callback) {
	if (!uuid) callback('No id provided.', null);
	Users.findOne({
		uuid: uuid
	}, function(err, user) {
		if (err) callback(err);
		if (!user) callback('User not found.', null);
		else {
			callback(null, user.transactions[user.transactions.length - 1]);
		}
	});
}

module.exports.updateBalance = function(uuids, uuidr, amount, callback) {
	if (!uuids || !uuidr) return callback('No id provided.');
	Users.findOne({
		uuid: uuids
	}, function(err, sender) {
		if (err) return callback(err);
		if (!sender) return callback("Sender not found.");
		Users.findOne({
			uuid: uuidr
		}, function(err, reciever) {
			if (err) return callback(err);
			if (!reciever) return callback("Reciever not found.");
			if (sender.balance < parseInt(amount)) {
				callback("اعتبار کافی نیست.");
			} else {

				sender.balance -= parseInt(amount);
				var newTransaction1 = {
					amount: amount,
					description: "پرداخت",
					date: new Date()
				}
				sender.transactions.push(newTransaction1);
				sender.save(function(err) {
					if (err) return callback(err);
				});
				var newTransaction2 = {
					amount: amount,
					description: "دریافت",
					date: new Date()
				}
				reciever.transactions.push(newTransaction2);
				reciever.balance += parseInt(amount);
				reciever.save(function(err) {
					if (err) return callback(err);
				});
				callback();
			}
		});
	});
}