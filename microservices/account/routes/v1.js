var express = require('express');
var router = express.Router();
var User = require('../models/user.js');
var jwt = require('jsonwebtoken');
const redis = require("redis");
const client = redis.createClient();
const secretKey = 'sg92pjfslGq2r6jglkAH3fdSGlk2jRFfaSDF32F@#RSdfralaidjliLJW';

/*
	501: add user error
	502: invalid token
	503: raise balance error
	504: get balance error
	505: get history error
	506: get last error
	507: update error
*/

function ensureAuth(req, res, next) {
	var token = req.headers.token;
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			client.get(decoded.username, function(err, result) {
				if (decoded.uuid == result) {
					return next();
				} else {
					res.ok(false, 'Auth failed', 401);
				}
			});
		}
	});
}


router.post('/adduser', function(req, res, next) {
	var uuid = req.body.uuid;
	var newUser = User({
		uuid: uuid,
		balance: 0
	});
	if (!uuid) return res.ok(false, "Bad Request", 400)
	User.addUser(newUser, function(err) {
		if (err) {
			res.ok(false, err.message, 501);
		} else{
			res.ok(true);
		}
	})
});


router.put('/raisecredit', ensureAuth, function(req, res, next) {
	var amount = req.body.amount;
	var token = req.headers.token;
	if (!amount || !token) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			User.raiseBalance(decoded.uuid, amount, function(err) {
				if (err) res.ok(false, err, 503);
				else {
					res.ok(true);
				}
			});
		}
	});

});

router.get('/getbalance', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	if (!token) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			User.getBalance(decoded.uuid, function(err, balance) {
				if (err) return res.ok(false, err, 504);
				else {
					res.ok({
						balance: balance
					});
				}
			});
		}
	});
});

router.get('/gethistory/:n', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	var n = req.params.n;
	if (!token || !n) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			User.getHistory(decoded.uuid, n, function(err, transactions) {
				if (err) res.ok(false, err, 505);
				else{
					res.ok({
						transactions: transactions
					});
				}
			});
		}
	});
});

router.get('/getlasttransaction', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	if (!token) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			User.getLastTransaction(decoded.uuid, function(err, lasttransaction) {
				if (err) res.ok(false, err, 506);
				else{
					res.ok({
						lasttransaction: lasttransaction
					});
				}
			});
		}
	});

});

router.get('/lasttransaction', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	if (!token) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			User.getLastTransaction(decoded.uuid, function(err, lasttransaction) {
				if (err) res.ok(false, err, 506);
				else{
					res.ok({
						lasttransaction: lasttransaction
					});
				}
			});
		}
	});
});



router.put('/updatebalance', function(req, res, next) {
	var uuids = req.body.uuids;
	var uuidr = req.body.uuidr;
	var amount = req.body.amount;
	if (!uuids || !uuidr || !amount) return res.ok(false, "Bad Request.", 400);
	User.updateBalance(uuids, uuidr, amount, function(err) {
		if (err) {
			return res.ok(false, err, 507);
		} else {
			return res.ok(true);
		}
	});
});


module.exports = router;