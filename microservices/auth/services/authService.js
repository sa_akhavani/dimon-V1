const db = require('../models');
const userService = require('./userService.js');
const Promise = require("bluebird");
const uuid = require('uuid');
const redis = require("redis");
const client = redis.createClient();
const jwt = Promise.promisifyAll(require('jsonwebtoken'));
var querystring = require('querystring');
var http = require('http');

const secretKey = 'sg92pjfslGq2r6jglkAH3fdSGlk2jRFfaSDF32F@#RSdfralaidjliLJW';

var auth = {};

function createUserData(user) {
	return {
		username: user.username,
		id: user.id,
		phone: user.phone,
		role: user.role,
		permission: user.permission,
		uuid: user.uid,
		name: user.name
	}
}

function createSession(user) {
	var sessionId = user.uid;
	client.set(user.username, sessionId);
	//set expire time
}
auth.login = function(username, password) {
	return userService.findByUsernameAndPassword(username, password)
		.then(function(user) {
			if (!user) return null;
			var userData = createUserData(user);
			createSession(user);
			return jwt.signAsync(userData, secretKey, {
				expiresIn: 24 * 3600
			}); //24 hours
		});
}

module.exports = auth;