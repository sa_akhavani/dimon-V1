const db = require('../models');
const Promise = require("bluebird")
const bcrypt = Promise.promisifyAll(require('bcrypt'));
const uuid = require('uuid');
const sms = require('./sms');

var querystring = require('querystring');
var http = require('http');

function randomNumber(digits) {
  var min = Math.pow(10, digits - 1);
  var max = min * 10;
  return Math.floor(min + Math.random() * (max - min));
}

function addUsertoAccountingDB(uuid, callback) {
  var data = querystring.stringify({
    uuid: uuid
  });
  var options = {
    host: 'localhost',
    port: 4000, //???
    path: '/v1/adduser',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(data)
    }
  };
  var req = http.request(options, function(res) {
    if (res.statusCode == 200) {
      callback();
    } else {
      callback("error");
    }

    res.setEncoding('utf8');
    res.on('data', function(chunk) {
      // console.log("body: " + chunk);
    });
  });
  req.write(data);
  req.end();
}

module.exports = {
  create: function(username, password, phone, role) {
    return db.User.findOne({
      where: {
        username: username,
        active: true
          // phone: phone
      }
    }).then(function(user) {
      if (user) {
        return true;
      } else {
        return db.User.findOne({
          where: {
            username: username,
            active: false
              // phone: phone
          }
        }).then(function(user) {
          var code = randomNumber(4);
          if (user) {
            user.phoneCode = code;
            user.save().then(function() {});
          } else {
            return bcrypt.genSaltAsync(10)
              .then(function(salt) {
                return bcrypt.hashAsync(password, salt)
              })
              .then(function(hash) {
                password = hash;
                sms.send({
                  message: "کد تایید حساب شما: " + code.toString(),
                  to: phone
                }, function(send, status) {
                  console.log(send);
                  console.log(status);
                });
                return db.User.create({
                  username,
                  password,
                  tempPhone: phone,
                  phoneCode: code,
                  role: role,
                  uid: uuid.v4()
                });
              });
          }
        });
      }
    });
  },
  verify: function(phone, code, deviceId, simSerial) {
    return db.User.findOne({
        where: {
          tempPhone: phone,
          phoneCode: code
        }
      })
      .then(function(user) {
        if (!user) return false;
        else {
          user.active = true;
          user.phone = phone;
          user.tempPhone = null;
          user.code = null;
          user.deviceId = deviceId;
          user.simSerial = simSerial;
          user.save().then(function(result) {
            // console.log(result);
            addUsertoAccountingDB(result.uid, function(err) {
              if (err) return false;
              else {
                return true;
              }
            });
          })
        }
      });
  },
  findByUsernameAndPassword: function(username, password) {
    return db.User.findOne({
        where: {
          username: username,
          active: true
        }
      })
      .then(function(user) {
        if (!user) return null;
        return bcrypt.compareAsync(password, user.password)
          .then(function(equals) {
            if (!equals) return null;
            return user;
          })
      })
  },
  getInfo: function(username, callback) {
    return db.User.findOne({
        where: {
          username: username,
          active: true
        }
      })
      .then(function(user) {
        if (!user) return callback("User not found", null);
        else {
          return callback(null, user.name, user.nationalId, user.birthdate);
        }
      });
  },
  senderUpdateProfile: function(username, name, nationalId, birthdate, callback) {
    return db.User.findOne({
        where: {
          username: username,
          active: true
        }
      })
      .then(function(user) {
        if (!user) return callback("User not found");
        else {
          user.name = name;
          user.nationalId = nationalId;
          user.birthdate = birthdate;
          user.save().then(callback);
        }
      });
  },
  recieverUpdateProfile: function(username, name, nationalId, birthdate, shabaNumber, frontPhoto, backPhoto, callback) {
    return db.User.findOne({
        where: {
          username: username,
          active: true
        }
      })
      .then(function(user) {
        if (!user) return callback("User not found");
        else {
          user.name = name;
          user.nationalId = nationalId;
          user.birthdate = birthdate;
          user.shabaNumber = shabaNumber;
          user.frontPhoto = frontPhoto;
          user.backPhoto = backPhoto;
          user.permission = 1;
          user.save().then(callback);
        }
      });
  },
  resetPassword: function(username, lastpassword, newpassword, callback) {
    db.User.findOne({
        where: {
          username: username,
          active: true
        }
      })
      .then(function(user) {
        if (!user) return null;
        return bcrypt.compareAsync(lastpassword, user.password)
          .then(function(equals) {
            if (!equals) callback("Passwords do not match");
            else {
              return bcrypt.genSaltAsync(10)
                .then(function(salt) {
                  return bcrypt.hashAsync(newpassword, salt)
                })
                .then(function(hash) {
                  user.password = hash;
                  user.save().then(callback);
                });
            }
          });
      });
  }
}