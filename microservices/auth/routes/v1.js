var express = require('express');
var router = express.Router();
var userService = require('../services/userService.js');
var authService = require('../services/authService.js');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
const redis = require("redis");
const client = redis.createClient();
var fs = require('fs');
const secretKey = 'sg92pjfslGq2r6jglkAH3fdSGlk2jRFfaSDF32F@#RSdfralaidjliLJW';

function ensureAuth(req, res, next) {
	var token = req.headers.token;
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			client.get(decoded.username, function(err, result) {
				if (decoded.uuid == result) {
					return next();
				} else {
					res.ok(false, 'Auth failed', 401);
				}
			});
		}
	});
}

router.post('/register', function(req, res, next) {
	var {
		phone,
		password,
		role
	} = req.body;
	if (!phone || !password) return res.ok(false, "Bad Request.", 400);
	userService.create(phone, password, phone, role)
		.then(function(problem) {
			// console.log(problem);
			if (problem == true) {
				res.ok(false, "User Exists", 515);
			} else {
				res.ok(true);
			}
		}).catch(function() {
			res.ok(false, "Failed to register", 515);
		});
});

router.post('/verify', function(req, res, next) {
	var {
		phone,
		code,
		device,
		sim
	} = req.body;
	if (!phone || !code || !device || !sim) return res.ok(false, "Bad Request.", 400);
	userService.verify(phone, code, device, sim)
		.then(function(verified) {
			console.log(verified);
			if (verified==undefined) return res.ok(true);
			else  {
				return res.ok(false, "Server Error", 500);
			}
		}).catch(function(){
				return res.ok(false, "Server Error", 500);
		});
});

router.post('/login', function(req, res, next) {
	var {
		username,
		password
	} = req.body;
	if (!username || !password) return res.ok(false, "Bad Request.", 400);
	authService.login(username, password)
		.then(function(token) {
			if (!token) return res.ok(false, "User Not Found", 500);
			else {
				res.ok({
					token: token
				});
			}
		});
});

router.put('/resetpassword', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	var lastpassword = req.body.lastpassword;
	var newpassword = req.body.newpassword;
	if (!token || !lastpassword || !newpassword) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			userService.resetPassword(decoded.username, lastpassword, newpassword, function(err) {
				if (err) return res.ok(false, err, 513);
				else {
					res.ok(true);
				}
			})
		}
	});
});

router.get('/getinfo', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	if (!token) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			userService.getInfo(decoded.username, function(err, name, nationalId, birthdate) {
				if (err) return res.ok(false, err, 511);
				else {
					if (!name && !nationalId && !birthdate) {
						return res.ok(false, err, 511);
					} else {
						return res.ok({
							name: name,
							nationalId: nationalId,
							birthdate: birthdate
						});
					}
				}
			});
		}
	});
});

router.put('/senderupdateprofile', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	var {
		name,
		nationalId,
		birthdate
	} = req.body;
	if (!token || !name || !nationalId || !birthdate) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			userService.senderUpdateProfile(decoded.username, name, nationalId, birthdate, function(user, err) {
				if (err) res.ok(false, err, 513);
				else {
					res.ok(true);
				}
			});
		}
	});
});

router.put('/recieverupdateprofile', ensureAuth, function(req, res, next) {
	var token = req.headers.token;
	var {
		name,
		nationalId,
		birthdate,
		shabaNumber,
		frontPhoto,
		backPhoto
	} = req.body;
	if (!token || !name || !nationalId || !birthdate || !frontPhoto || !backPhoto || !shabaNumber) return res.ok(false, "Bad Request.", 400);
	jwt.verify(token, secretKey, function(err, decoded) {
		if (err) return res.ok(false, err, 502);
		else {
			userService.recieverUpdateProfile(decoded.username, name, nationalId, birthdate, shabaNumber, frontPhoto, backPhoto, function(user, err) {
				if (err) res.ok(false, err, 513);
				else {
					res.ok(true);
				}
			});
		}
	});
});




module.exports = router;