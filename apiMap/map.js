/**
 * @api {post} auth.dimon.ir/v1/login Login
 * @apiVersion 0.1.0
 * @apiName Login
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiParam {String} username
 * @apiParam {String} password
 *
 * @apiSuccess {Boolean}  success           Success Flag
 * @apiSuccess {Object}   result            Result
 * @apiSuccess {String}   result.token      A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 * @apiSuccessExample {json} Success-Response
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "result": {
 *          token: "abcd.efghi.jkl"
 *       }
 *     }
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 *
 * @apiSampleRequest http://auth.dimon.ir/v1/login
 *
 */
function login() { return; }

/**
 * @api {post} auth.dimon.ir/v1/register Register a new user
 * @apiVersion 0.1.0
 * @apiName Register
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiParam {String} phone Phone Number
 * @apiParam {String} password Password
 * @apiParam {String="sender","receiver"} Role User role: sender/reciever
 *
 * @apiSuccess {Boolean} success 	Success Flag
 * @apiSuccess {Boolean} result 	Success Flag
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://auth.dimon.ir/v1/register
 */
function register() { return; }


/**
 * @api {get} auth.dimon.ir/v1/getinfo Get info of a user
 * @apiVersion 0.1.0
 * @apiName Get Info
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiHeader {String} token   	   A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 *
 * @apiSuccess {bool} success
 * @apiSuccess {object} result
 * @apiSuccess {String} result.name 
 * @apiSuccess {String} result.nationalId 
 * @apiSuccess {String} result.birthdate 
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *		 "result": {
 *			"name": "ali",
 *			"nationalId": "1235456",
 *			"birthdate": "28/05/95"
 *		 }
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://auth.dimon.ir/v1/register
 */
function getinfo() { return; }



/**
 * @api {post} auth.dimon.ir/v1/verify Verify registered user
 * @apiVersion 0.1.0
 * @apiName Verify
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiParam {String} phone Phone Number
 * @apiParam {String} code Confirmation Code
 * @apiParam {String} publicKey Public key generated automatically
 * @apiParam {String} device Device ID (e.g. IMEI)
 * @apiParam {String} sim SimCard Serial Number
 *
 * @apiSuccess {Boolean} success         Success Flag
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://auth.dimon.ir/v1/verify
 */
function verify() { return; }


/**
 * @api {post} auth.dimon.ir/v1/resetpassword resetpassword of user
 * @apiVersion 0.1.0
 * @apiName Reset Password
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiHeader {String} token   	   A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 *
 * @apiParam {String} lastpassword
 * @apiParam {String} newpassword
 *
 * @apiSuccess {bool} success         Success Flag
 * @apiSuccess {bool} result

 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
		 "result": true
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://auth.dimon.ir/v1/verify
 */
function resetpassword() { return; }


/**
 * @api {put} auth.dimon.ir/v1/senderupdateprofile Sender update profile
 * @apiVersion 0.1.0
 * @apiName Sender Update Profile
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiHeader {String} token   	   A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 *
 * @apiParam {String} name
 * @apiParam {String} nationalId
 * @apiParam {String} birthdate
 *
 * @apiSuccess {bool} success         Success Flag
 * @apiSuccess {bool} result
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *		 "result": true
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://auth.dimon.ir/v1/verify
 */
function senderupdateprofile() { return; }


/**
 * @api {put} auth.dimon.ir/v1/recieverupdateprofile Reciever update profile
 * @apiVersion 0.1.0
 * @apiName Reciever Update Profile
 * @apiGroup Auth
 * @apiPermission none
 *
 * @apiHeader {String} token   	   A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 *
 * @apiParam {String} name
 * @apiParam {String} nationalId
 * @apiParam {String} birthdate
 * @apiParam {String} shabaNumber
 * @apiParam {String} frontPhoto
 * @apiParam {String} backPhoto
 *
 * @apiSuccess {bool} success         Success Flag
 * @apiSuccess {bool} result

 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
		 "result": true
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://auth.dimon.ir/v1/verify
 */
function recieverupdateprofile() { return; }



/**
 * @api {get} acc.dimon.ir/v1/gethistory?count=n Get Transactions History
 * @apiVersion 0.1.0
 * @apiName History
 * @apiGroup Account
 * @apiPermission none
 *
 * @apiParam {Number} count Number of Transactions
 *
 * @apiSuccess {Boolean} success         Success Flag
 * @apiSuccess {Object[]} transactions   List of Transactions
 * @apiSuccess {Number} transactions.ammount   Ammount of Money
 * @apiSuccess {String} transactions.desciption Description of Transaction
 * @apiSuccess {String} transactions.date   Date
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 * 		 "transactions" = [
 *			{
 *				"ammount": 100,
 *				"description": "My Money",
 *				"date": "28/5/1374"
 *			},
 *			{
 *				"ammount": 200,
 *				"description": "My Second Money",
 *				"date": "28/5/1375"
 *			}
  *		  ]
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://acc.dimon.ir/v1/gethistory
 */


function history() {return;}

/**
 * @api {put} acc.dimon.ir/v1/raisecredit Raise Credit 
 * @apiVersion 0.1.0
 * @apiName Raise Credit
 * @apiGroup Account
 * @apiPermission none
 *
 * @apiHeader {String} token   	   A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 * @apiParam {Number} amount amount of money
 *
 *
 * @apiSuccess {Boolean} success       Success Flag
 * @apiSuccess {Object} transaction       transaction
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 * 		 "transaction" = [
 *			{
 *				"ammount": 100,
 *				"description": "My Money",
 *				"date": "28/5/1374"
 *			}
  *		  ]
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 * @apiSampleRequest http://acc.dimon.ir/v1/raisecredit
 */



function raiseCredit() { return; }


/**
 * @api {get} acc.dimon.ir/v1/getlasttransaction Get Last Transaction
 * @apiVersion 0.1.0
 * @apiName Get Last Transaction
 * @apiGroup Account
 * @apiPermission none
 *
 * @apiHeader {String} token A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 * 
 *
 * @apiSuccess {Boolean} success       Success Flag
 * @apiSuccess {Object} transaction       Last Transaction
 * @apiSuccess {Number} transactions.ammount   Ammount of Money
 * @apiSuccess {String} transactions.desciption Description of Transaction
 * @apiSuccess {String} transactions.date   Date

 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 		 "transaction": {
			"amount": "12345",
			"desciption": "poole Akhar",
			"date": "28/5/1374"
 		 }
 *     }
 *
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 * 		HTTP/1.1 400 Bad request
 */


 function getlasttransaction() {return;}

 /**
 * @api {get} acc.dimon.ir/v1/getbalance Get Balance
 * @apiVersion 0.1.0
 * @apiName Get Balance
 * @apiGroup Account
 * @apiPermission none
 *
 * @apiHeader {String} token A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 *
 *
 * @apiSuccess {Boolean} success       Success Flag
 * @apiSuccess {Number} balance Money

 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "balance": "100"
 *     }
 *
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 400 Bad request
 */


 function getbalance() {return;}


 /**
 * @api {post} core.dimon.ir/v1/getsound Get Sound
 * @apiVersion 0.1.0
 * @apiName Get Sound
 * @apiGroup Core
 * @apiPermission none
 *
 * @apiHeader {String} token A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 * @apiParam {Number} amount amount of money in the sound
 * 
 *
 * @apiSuccess {Boolean} success       Success Flag
 * @apiSuccess {File} sound sound name: transmit_{id}_{Date ddMMyy}_{Time hhmmss}.wav

 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *       "file": "transmit_2_100512_135530.wav"
 *     }
 *
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 400 Bad request
 */

 function getSound() {return;}


 /**
 * @api {post} core.dimon.ir/v1/sendsound Send Sound
 * @apiVersion 0.1.0
 * @apiName send Sound
 * @apiGroup Core
 * @apiPermission none
 *
 * @apiHeader {String} token A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 * @apiParam {File} sound sound containing id, rnd, amount
 * 
 *
 * @apiSuccess {Boolean} success 	Success Flag
 * @apiSuccess {Boolean} result 	result
 * @apiSuccess {String} error 	result
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *		 "result": true,
 		 "error": null
 *     }
 *
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *		 "result": false,
 		 "error": "error message"
 *     }
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 400 Bad request
 */

 function sendSound() {return;}

 /**
 * @api {get} core.dimon.ir/v1/getlist Get list
 * @apiVersion 0.1.0
 * @apiName Get list
 * @apiGroup Core
 * @apiPermission none
 *
 * @apiHeader {String} token A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 * 
 *
 * @apiSuccess {Boolean} success 	Success Flag
 * @apiSuccess {Object} payment list of payments
 * @apiSuccess {Number} payment.rid       
 * @apiSuccess {Number} payment.sid
 * @apiSuccess {Number} payment.rnd
 * @apiSuccess {Number} payment.amount
 * @apiSuccess {String} payment.rname
 * @apiSuccess {String} payment.ruuid
 * @apiSuccess {Number} payment.status always 1
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *		 "list": [{
 *				"rid": 123,
 *				"sid": 123,
 *				"rnd": 123,
 *				"amount": 1000,
 *				"rname": "john doe",
 *				"ruuid": "ccc-ccc-ccc-ccc"
 *				"status": 1
 * 			}];
 *     }
 *
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 400 Bad request
 */

 function getlist() {return;}




  /**
 * @api {post} core.dimon.ir/v1/verifyreceiver Verify Reciever
 * @apiVersion 0.1.0
 * @apiName Verify Reciever
 * @apiGroup Core
 * @apiPermission none
 *
 * @apiHeader {String} token A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid
 * 
 *
 *
 * @apiParam {Number} ruuid       
 * @apiParam {Number} sid
 * @apiParam {Number} rnd
 * @apiParam {Number} amount
 * @apiParam {String} rname
 *
 * @apiSuccess {Boolean} success 	Success Flag
 * @apiSuccess {Boolean} result 	Result Flag
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": true,
 *		 "result": true,
 *     }
 *
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 500 Internal Server Error
 * @apiErrorExample {json} Register error
 *              HTTP/1.1 400 Bad request
 */
 function verifyreceiver() {return;}