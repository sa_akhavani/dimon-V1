define({
  "name": "dimon-docs",
  "version": "0.1.0",
  "description": "Dimon API docs",
  "title": "API docs for Dimon",
  "sampleUrl": "https://api.dimon.com/v1",
  "template": {},
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-09-10T08:43:34.400Z",
    "url": "http://apidocjs.com",
    "version": "0.16.1"
  }
});
