define({ "api": [
  {
    "type": "get",
    "url": "acc.dimon.ir/v1/getbalance",
    "title": "Get Balance",
    "version": "0.1.0",
    "name": "Get_Balance",
    "group": "Account",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "balance",
            "description": "<p>Money</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"balance\": \"100\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "filename": "apiMap/map.js",
    "groupTitle": "Account",
    "sampleRequest": [
      {
        "url": "https://api.dimon.com/v1acc.dimon.ir/v1/getbalance"
      }
    ]
  },
  {
    "type": "get",
    "url": "acc.dimon.ir/v1/getlasttransaction",
    "title": "Get Last Transaction",
    "version": "0.1.0",
    "name": "Get_Last_Transaction",
    "group": "Account",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "transaction",
            "description": "<p>Last Transaction</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "transactions.ammount",
            "description": "<p>Ammount of Money</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transactions.desciption",
            "description": "<p>Description of Transaction</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transactions.date",
            "description": "<p>Date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n \t\t \"transaction\": {\n\t\t\t\"amount\": \"12345\",\n\t\t\t\"desciption\": \"poole Akhar\",\n\t\t\t\"date\": \"28/5/1374\"\n \t\t }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "filename": "apiMap/map.js",
    "groupTitle": "Account",
    "sampleRequest": [
      {
        "url": "https://api.dimon.com/v1acc.dimon.ir/v1/getlasttransaction"
      }
    ]
  },
  {
    "type": "get",
    "url": "acc.dimon.ir/v1/gethistory?count=n",
    "title": "Get Transactions History",
    "version": "0.1.0",
    "name": "History",
    "group": "Account",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>Number of Transactions</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "transactions",
            "description": "<p>List of Transactions</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "transactions.ammount",
            "description": "<p>Ammount of Money</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transactions.desciption",
            "description": "<p>Description of Transaction</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "transactions.date",
            "description": "<p>Date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"transactions\" = [\n\t\t\t{\n\t\t\t\t\"ammount\": 100,\n\t\t\t\t\"description\": \"My Money\",\n\t\t\t\t\"date\": \"28/5/1374\"\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"ammount\": 200,\n\t\t\t\t\"description\": \"My Second Money\",\n\t\t\t\t\"date\": \"28/5/1375\"\n\t\t\t}\n\t\t  ]\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://acc.dimon.ir/v1/gethistory"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Account"
  },
  {
    "type": "put",
    "url": "acc.dimon.ir/v1/raisecredit",
    "title": "Raise Credit",
    "version": "0.1.0",
    "name": "Raise_Credit",
    "group": "Account",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>amount of money</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "transaction",
            "description": "<p>transaction</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"transaction\" = [\n\t\t\t{\n\t\t\t\t\"ammount\": 100,\n\t\t\t\t\"description\": \"My Money\",\n\t\t\t\t\"date\": \"28/5/1374\"\n\t\t\t}\n\t\t  ]\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://acc.dimon.ir/v1/raisecredit"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Account"
  },
  {
    "type": "get",
    "url": "auth.dimon.ir/v1/getinfo",
    "title": "Get info of a user",
    "version": "0.1.0",
    "name": "Get_Info",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "success",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "object",
            "optional": false,
            "field": "result",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.name",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.nationalId",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.birthdate",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"result\": {\n\t\t\t\"name\": \"ali\",\n\t\t\t\"nationalId\": \"1235456\",\n\t\t\t\"birthdate\": \"28/05/95\"\n\t\t }\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://auth.dimon.ir/v1/register"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "auth.dimon.ir/v1/login",
    "title": "Login",
    "version": "0.1.0",
    "name": "Login",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>Result</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result.token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"result\": {\n     token: \"abcd.efghi.jkl\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://auth.dimon.ir/v1/login"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Auth"
  },
  {
    "type": "put",
    "url": "auth.dimon.ir/v1/recieverupdateprofile",
    "title": "Reciever update profile",
    "version": "0.1.0",
    "name": "Reciever_Update_Profile",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nationalId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthdate",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "shabaNumber",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "frontPhoto",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "backPhoto",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "result",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"result\": true\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://auth.dimon.ir/v1/verify"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "auth.dimon.ir/v1/register",
    "title": "Register a new user",
    "version": "0.1.0",
    "name": "Register",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"sender\"",
              "\"receiver\""
            ],
            "optional": false,
            "field": "Role",
            "description": "<p>User role: sender/reciever</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "result",
            "description": "<p>Success Flag</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://auth.dimon.ir/v1/register"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "auth.dimon.ir/v1/resetpassword",
    "title": "resetpassword of user",
    "version": "0.1.0",
    "name": "Reset_Password",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastpassword",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "newpassword",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "result",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"result\": true\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://auth.dimon.ir/v1/verify"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Auth"
  },
  {
    "type": "put",
    "url": "auth.dimon.ir/v1/senderupdateprofile",
    "title": "Sender update profile",
    "version": "0.1.0",
    "name": "Sender_Update_Profile",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nationalId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "birthdate",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "bool",
            "optional": false,
            "field": "result",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"result\": true\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://auth.dimon.ir/v1/verify"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "auth.dimon.ir/v1/verify",
    "title": "Verify registered user",
    "version": "0.1.0",
    "name": "Verify",
    "group": "Auth",
    "permission": [
      {
        "name": "none"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Confirmation Code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "publicKey",
            "description": "<p>Public key generated automatically</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device",
            "description": "<p>Device ID (e.g. IMEI)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sim",
            "description": "<p>SimCard Serial Number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "http://auth.dimon.ir/v1/verify"
      }
    ],
    "filename": "apiMap/map.js",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "core.dimon.ir/v1/getsound",
    "title": "Get Sound",
    "version": "0.1.0",
    "name": "Get_Sound",
    "group": "Core",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>amount of money in the sound</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "File",
            "optional": false,
            "field": "sound",
            "description": "<p>sound name: transmit_{id}<em>{Date ddMMyy}</em>{Time hhmmss}.wav</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true,\n  \"file\": \"transmit_2_100512_135530.wav\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "filename": "apiMap/map.js",
    "groupTitle": "Core",
    "sampleRequest": [
      {
        "url": "https://api.dimon.com/v1core.dimon.ir/v1/getsound"
      }
    ]
  },
  {
    "type": "get",
    "url": "core.dimon.ir/v1/getlist",
    "title": "Get list",
    "version": "0.1.0",
    "name": "Get_list",
    "group": "Core",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "payment",
            "description": "<p>list of payments</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment.rid",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment.sid",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment.rnd",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment.amount",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "payment.rname",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "payment.ruuid",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment.status",
            "description": "<p>always 1</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"list\": [{\n\t\t\t\t\"rid\": 123,\n\t\t\t\t\"sid\": 123,\n\t\t\t\t\"rnd\": 123,\n\t\t\t\t\"amount\": 1000,\n\t\t\t\t\"rname\": \"john doe\",\n\t\t\t\t\"ruuid\": \"ccc-ccc-ccc-ccc\"\n\t\t\t\t\"status\": 1\n\t\t\t}];\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "filename": "apiMap/map.js",
    "groupTitle": "Core",
    "sampleRequest": [
      {
        "url": "https://api.dimon.com/v1core.dimon.ir/v1/getlist"
      }
    ]
  },
  {
    "type": "post",
    "url": "core.dimon.ir/v1/sendsound",
    "title": "Send Sound",
    "version": "0.1.0",
    "name": "send_Sound",
    "group": "Core",
    "permission": [
      {
        "name": "none"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>A JWT token will be used for authentication and authorization in all requests. This token has phoneNo, username, type, permission, sid, and aid</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "sound",
            "description": "<p>sound containing id, rnd, amount</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "success",
            "description": "<p>Success Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "result",
            "description": "<p>result</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "error",
            "description": "<p>result</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"result\": true,\n \t\t \"error\": null\n    }\n\n    HTTP/1.1 200 OK\n    {\n      \"success\": true,\n\t\t \"result\": false,\n \t\t \"error\": \"error message\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Register error",
          "content": "HTTP/1.1 400 Bad request",
          "type": "json"
        }
      ]
    },
    "filename": "apiMap/map.js",
    "groupTitle": "Core",
    "sampleRequest": [
      {
        "url": "https://api.dimon.com/v1core.dimon.ir/v1/sendsound"
      }
    ]
  }
] });
